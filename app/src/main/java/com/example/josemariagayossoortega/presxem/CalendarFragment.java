package com.example.josemariagayossoortega.presxem;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.exceptions.OutOfDateRangeException;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendarFragment extends Fragment {

    //Atributos
    private CalendarView mCalendario;
    private List<EventDay> mEventDay = new ArrayList<>();
    private String TAG= "Fragment calendar";
    Context mContext;

    public CalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);
        mCalendario= (CalendarView) view.findViewById(R.id.MaterialCalendarView);
        initialize();

        return  view;
    }

    private void initialize(){
        Calendar calendar = Calendar.getInstance();
        try {
            mCalendario.setDate(calendar); //estabelcer la pagina en la fecha actual
        } catch (OutOfDateRangeException e) {
            e.printStackTrace();
        }
        //get current date
        mEventDay.add(new EventDay(calendar, R.drawable.ic_menu_camera));
        mCalendario.setEvents(mEventDay);

        //set on click from day
        mCalendario.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                Log.d(TAG, "onDayClick: "+eventDay.toString().trim());
                //Toast.makeText(getApplicationContext(), eventDay.getCalendar().getTime().toString(),Toast.LENGTH_SHORT).show();

                Calendar calendar = eventDay.getCalendar();
                mEventDay.add(new EventDay(calendar, R.drawable.ic_menu_camera));//setI con
                mCalendario.setEvents(mEventDay);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
