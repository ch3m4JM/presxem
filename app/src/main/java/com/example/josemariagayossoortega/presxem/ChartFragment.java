package com.example.josemariagayossoortega.presxem;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

public class ChartFragment extends Fragment {

    LineChart chart;
    private String TAG=" ChartFragment";

    public ChartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_chart, container, false);

        // in this example, a LineChart is initialized from xml
        chart = (LineChart) view.findViewById(R.id.chart);
        inizialiteChart();
        return view;
    }

    private void inizialiteChart() {
        Coordenada[] dataTest = {new Coordenada(2,4), new Coordenada(4,2), new Coordenada(3,5)} ;

        List<Entry> entries = new ArrayList<Entry>();

        for (Coordenada data : dataTest) {

            // turn your data into Entry objects
            entries.add(new Entry(data.getEjeX(), data.getEjeY()));
            Log.i(TAG, "inizialiteChart: "+data.getEjeY());
        }

        LineDataSet dataSet= new LineDataSet(entries,"Gastos");
        dataSet.setColor(Color.CYAN);

        LineData lineData = new LineData(dataSet);
        chart.setData(lineData);
        chart.invalidate(); //refresh
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private class Coordenada{
        private int ejeX,ejeY;

        public Coordenada(int ejeX, int ejeY) {
            this.ejeX = ejeX;
            this.ejeY = ejeY;
        }

        public int getEjeX() {
            return ejeX;
        }

        public void setEjeX(int ejeX) {
            this.ejeX = ejeX;
        }

        public int getEjeY() {
            return ejeY;
        }

        public void setEjeY(int ejeY) {
            this.ejeY = ejeY;
        }
    }
}
