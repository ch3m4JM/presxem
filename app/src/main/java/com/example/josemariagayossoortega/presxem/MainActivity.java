package com.example.josemariagayossoortega.presxem;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.DatePicker;
import com.applandeo.materialcalendarview.builders.DatePickerBuilder;
import com.applandeo.materialcalendarview.listeners.OnSelectDateListener;
import com.example.josemariagayossoortega.presxem.adapters.MyPagerAdapter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Context mContext;
    private String TAG = "Main activity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mContext = this;

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                showCalendarDialog();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Toast.makeText(this, "Aplicación iniciada!", Toast.LENGTH_SHORT).show();
        //Crashlytics.getInstance().crash(); // Force a crash

        MyPagerAdapter myPagerAdapter =
                new MyPagerAdapter(
                        getSupportFragmentManager());
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(myPagerAdapter);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void showCalendarDialog() {
        DatePickerBuilder builder = new DatePickerBuilder(mContext, new OnSelectDateListener() {
            @Override
            public void onSelect(List<Calendar> calendar) {
                String fechas = "";
                for (int i = 0; i < calendar.size(); i++) {
                    Date fecha = calendar.get(i).getTime();
                    int dia = fecha.getDate();
                    int mes = fecha.getMonth();
                    int año = fecha.getYear();

                    Log.d(TAG, "onSelect() returned: " + calendar.get(i).getTime().getDate());
                    fechas += "|| " + dia + "/" + mes + "/" + año;
                }
                Toast.makeText(getApplicationContext(), fechas, Toast.LENGTH_LONG).show();
                Log.i(TAG, "onSelect: " + fechas);
            }
        }).pickerType(CalendarView.RANGE_PICKER)
                .date(Calendar.getInstance())
                .headerColor(R.color.colorAccent) // Color of the dialog header
                .headerLabelColor(R.color.colorPrimary) // Color of the header label
                .previousButtonSrc(R.drawable.common_full_open_on_phone) // Custom drawable of the previous arrow
                .forwardButtonSrc(R.drawable.ic_menu_share) // Custom drawable of the forward arrow
                //.previousPageChangeListener(new OnCalendarPageChangeListener(){}) // Listener called when scroll to the previous page
                //.forwardPageChangeListener(new OnCalendarPageChangeListener(){}) // Listener called when scroll to the next page
                        /*.abbreviationsBarColor(R.color.colorPrimary) // Color of bar with day symbols
                        .abbreviationsLabelsColor(R.color.color) // Color of symbol labels
                        .pagesColor(R.color.sampleLighter) // Color of the calendar background
                        .selectionColor(R.color.color) // Color of the selection circle
                        .selectionLabelColor(R.color.color) // Color of the label in the circle
                        .daysLabelsColor(R.color.color) // Color of days numbers
                        .anotherMonthsDaysLabelsColor(R.color.color) // Color of visible days numbers from previous and next month page
                        .disabledDaysLabelsColor(R.color.color) // Color of disabled days numbers
                        .todayLabelColor(R.color.color) // Color of the today number*/
                .dialogButtonsColor(R.color.colorAccent); // Color of "Cancel" and "OK" button

        DatePicker datePicker = builder.build();
        datePicker.show();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
