package com.example.josemariagayossoortega.presxem.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.josemariagayossoortega.presxem.CalendarFragment;
import com.example.josemariagayossoortega.presxem.ChartFragment;

/**
 * Created by josemariagayossoortega on 15/05/18.
 */

public class MyPagerAdapter extends FragmentStatePagerAdapter {

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment;
        switch (i) {
            case 0:
                fragment = new CalendarFragment();
                break;
            case 1:
                fragment = new ChartFragment();
                break;
            default:
                fragment = null;
        }
        return fragment;
    }


    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Calendario";
            case 1:
                return "Grafica";
        }
        return null;
    }
}
